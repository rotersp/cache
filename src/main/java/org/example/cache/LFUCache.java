package org.example.cache;

import java.util.*;

/**
 * This class implements the {@code Cache} interface with LFU eviction strategy, backed by a hash table
 * (actually a {@code HashMap} instance).
 *
 * <p>This implementation has an algorithmic complexity of O (1)
 * for the {@code put} and {@code get} operations.</p>
 *
 * @param <K> the type of keys maintained by this map
 * @param <V> the type of mapped values
 */
public class LFUCache<K, V> implements Cache<K, V> {

    private static class Counter<K> {

        private int value;

        private Counter<K> upper;

        private Counter<K> lower;

        private Set<K> keys;
    }

    private final int capacity;

    private final Map<K, V> values;
    private final Map<K, Counter<K>> counters;

    private Counter<K> min;

    public LFUCache(int capacity) {
        assert capacity > 0;
        this.capacity = capacity;
        values = new HashMap<>(capacity);
        counters = new HashMap<>(capacity);
    }

    @Override
    public V get(K key) {
        if (!values.containsKey(key)) {
            return null;
        }
        updateCounters(key);
        return values.get(key);
    }

    @Override
    public boolean contains(K key) {
        return values.containsKey(key);
    }

    @Override
    public V put(K key, V value) {
        if (values.containsKey(key)) {
            values.put(key, value);
            updateCounters(key);
            return value;
        }
        if (values.size() == capacity) {
            evict();
        }
        values.put(key, value);
        if (min != null) {
            if (min.value == 1) {
                min.keys.add(key);
            } else {
                Counter<K> newCounter = createCounter(key);
                insertCounter(newCounter, min);
                min = newCounter;
            }
        } else {
            min = createCounter(key);
        }
        counters.put(key, min);
        return value;
    }

    private void updateCounters(K key) {
        Counter<K> oldCounter = counters.get(key);
        Counter<K> newCounter;
        if (oldCounter.upper != null && oldCounter.upper.value == oldCounter.value + 1) {
            newCounter = oldCounter.upper;
        } else {
            newCounter = new Counter<>();
            newCounter.value = oldCounter.value + 1;
            newCounter.keys = new HashSet<>();
            insertCounter(oldCounter, newCounter);
        }

        oldCounter.keys.remove(key);
        if (oldCounter.keys.isEmpty()) {
            deleteCounter(oldCounter);
            if (oldCounter == min) {
                min = newCounter;
            }
        }

        newCounter.keys.add(key);
        counters.put(key, newCounter);
    }

    private void deleteCounter(Counter<K> counter) {
        if (counter.upper != null) {
            counter.upper.lower = counter.lower;
        }
        if (counter.lower != null) {
            counter.lower.upper = counter.upper;
        }
    }

    private void insertCounter(Counter<K> prev, Counter<K> next) {
        if (prev.upper != null) {
            prev.upper.lower = next;
            next.upper = prev.upper;
        }
        prev.upper = next;
        next.lower = prev;
    }

    private Counter<K> createCounter(K key) {
        Counter<K> counter = new Counter<>();
        counter.value = 1;
        counter.keys = new HashSet<>();
        counter.keys.add(key);
        return counter;
    }

    private void evict() {
        K key = min.keys.iterator().next();
        values.remove(key);
        counters.remove(key);
        min.keys.remove(key);
        if (min.keys.isEmpty()) {
            deleteCounter(min);
            min = min.upper;
        }
    }
}
