package org.example.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * This class implements the {@code Cache} interface with LRU eviction strategy, backed by a hash table
 * (actually a {@code HashMap} instance).
 *
 * <p>This implementation has an algorithmic complexity of O (1)
 * for the {@code put} and {@code get} operations.</p>
 *
 * @param <K> the type of keys maintained by this map
 * @param <V> the type of mapped values
 */
public class LRUCache<K, V> implements Cache<K, V> {

    private static class Entry<K, V> {

        private final K key;

        private V value;

        private Entry<K, V> next;

        Entry(K key) {
            this.key = key;
        }
    }

    private final int capacity;

    private final Map<K, Entry<K, V>> map;

    private Entry<K, V> first;

    private Entry<K, V> last;

    public LRUCache(int capacity) {
        assert capacity > 0;
        this.capacity = capacity;
        map = new HashMap<>(capacity);
    }

    public V put(K key, V value) {
        if (map.containsKey(key)) {
            Entry<K, V> entry = map.get(key);
            entry.value = value;
            linkFirst(entry);
            return value;
        }
        if (map.size() == capacity) {
            evict();
        }

        Entry<K, V> entry = new Entry<>(key);
        entry.value = value;
        linkFirst(entry);
        map.put(key, entry);
        return entry.value;
    }

    public V get(K key) {
        if (!map.containsKey(key)) {
            return null;
        }
        Entry<K, V> entry = map.get(key);
        linkFirst(entry);
        return entry.value;
    }

    public boolean contains(K key) {
        return map.containsKey(key);
    }

    private void linkFirst(Entry<K, V> entry) {
        if (entry == first) {
            return;
        }
        Entry<K, V> f = first;
        first = entry;
        if (f == null) {
            last = first;
        } else {
            f.next = first;
            if (first == last) {
                last = first.next;
            }
        }
    }

    private void evict() {
        map.remove(last.key);
        last = last.next;
    }
}
