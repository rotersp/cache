package org.example.cache;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CacheTest {

    @Test
    public void testLRU() {
        Cache<String, Object> cache = new LRUCache<>(3);

        testCommonCases(cache);
        assertTrue(cache.contains("1"));
        assertFalse(cache.contains("2"));
    }

    @Test
    public void testLFU() {
        Cache<String, Object> cache = new LFUCache<>(3);

        testCommonCases(cache);
        assertTrue(cache.contains("2"));
        assertFalse(cache.contains("1"));
    }

    private void testCommonCases(Cache<String, Object> cache) {
        cache.put("1", 1);
        cache.put("2", 2);
        cache.put("3", 3);

        assertEquals(3, cache.get("3"));
        assertEquals(2, cache.get("2"));
        assertEquals(3, cache.put("3", 3));
        assertEquals(2, cache.get("2"));
        assertEquals(1, cache.get("1"));

        assertNull(cache.get("5"));

        cache.put("4", 4);

        assertTrue(cache.contains("3"));
        assertTrue(cache.contains("4"));
    }
}
